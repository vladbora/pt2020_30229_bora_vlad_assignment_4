package presentation;

import business.MenuItem;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class AdminGUI {

    private JFrame frame = new JFrame();
    private JButton btnCreate;
    private JButton btnEdit;
    private JButton btnDelete;
    private JButton btnCreateConfirm;
    private JButton btnCreateCompConfirm;
    private JButton btnDeleteConfirm;
    private JButton btnCreateCompAdd;
    private JButton btnEditBaseConfirm;
    private JButton btnMenu;
    private JButton btnEditCompConfirm;
    private JButton btnEditCompAdd;

    private JCheckBox checkComoposite;
    private JCheckBox checkCreateBase;
    private JCheckBox checkEditBase;
    private JCheckBox checkEditComposite;

    private JLabel labelCreateBaseMainText;
    private JLabel labelCreateBaseName;
    private JLabel labelCreateBasePrice;
    private JLabel labelCreateCompMainText;
    private JLabel labelCreateCompName;
    private JLabel labelCreateCompPrice;
    private JLabel labelDeleteName;
    private JLabel labelDeleteText;
    private JLabel labelEditBaseMainText;
    private JLabel labelEditBaseName;
    private JLabel labelEditBasePrice;
    private JLabel labelEditBaseCurrent;

    private JTextField textFieldCreateBaseName;
    private JTextField textFieldCreateBasePrice;
    private JTextField textFieldCreateCompName;
    private JTextField textFieldEditBaseName;
    private JTextField textFieldEditBasePrice;


    private JLabel labelEditCompMainText;
    private JLabel labelEditCompName;
    private JTextField textFieldEditCompName;
    private JLabel labelEditCompCurrent;

    private JComboBox comboCreteCompProduse;
    private JComboBox comboBoxDeleteItem;
    private JComboBox comboBoxEditBaseCombo;
    private JComboBox comboBoxEditCompCombo;
    private JComboBox comboBoxEditCompNew;

    private JTextArea textAreaCreateComp;
    private JTextArea textAreaEditComp;

    private JTable menuTable;
    private JScrollPane menuScrollPane;

    private ArrayList<MenuItem> compCurrentList = new ArrayList<MenuItem>();
    private ArrayList<MenuItem> compEditCurrentList = new ArrayList<MenuItem>();

    public AdminGUI()
    {
        this.initialize();
    }

    private void initialize() {
        frame.getContentPane().setBackground(Color.WHITE);
        frame.setBounds(300, 100, 850, 600);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.getContentPane().setLayout(null);
        frame.setTitle("Admin page");

        btnCreate = new JButton("Create");
        btnCreate.setBackground(Color.WHITE);
        btnCreate.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        btnCreate.setBounds(50, 50, 115, 40);
        frame.getContentPane().add(btnCreate);

        btnEdit = new JButton("Edit");
        btnEdit.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        btnEdit.setBackground(Color.WHITE);
        btnEdit.setBounds(50, 150, 115, 40);
        frame.getContentPane().add(btnEdit);

        btnDelete = new JButton("Delete");
        btnDelete.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        btnDelete.setBackground(Color.WHITE);
        btnDelete.setBounds(50, 250, 115, 40);
        frame.getContentPane().add(btnDelete);

        btnMenu = new JButton("Menu");
        btnMenu.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        btnMenu.setBackground(Color.WHITE);
        btnMenu.setBounds(50, 350, 115, 40);
        frame.getContentPane().add(btnMenu);

        checkComoposite = new JCheckBox("Composite Product");
        checkComoposite.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        checkComoposite.setBackground(Color.WHITE);
        checkComoposite.setBounds(200, 80, 115, 40);


        checkCreateBase = new JCheckBox("Base Product");
        checkCreateBase.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        checkCreateBase.setBackground(Color.WHITE);
        checkCreateBase.setBounds(200, 30, 115, 40);

        labelCreateBaseMainText = new JLabel("Introduceti numele si pretul pentru produsul dorit:");
        labelCreateBaseMainText.setFont(new Font("Times New Roman", Font.PLAIN, 15));
        labelCreateBaseMainText.setBounds(350, 0, 300, 40);

        labelCreateBaseName = new JLabel("Name:");
        labelCreateBaseName.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        labelCreateBaseName.setBounds(350, 50, 250, 20);

        labelCreateBasePrice = new JLabel("Price:");
        labelCreateBasePrice.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        labelCreateBasePrice.setBounds(350, 80, 250, 20);

        textFieldCreateBaseName = new JTextField();
        textFieldCreateBaseName.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        textFieldCreateBaseName.setBounds(400, 50, 200, 20);

        textFieldCreateBasePrice = new JTextField();
        textFieldCreateBasePrice.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        textFieldCreateBasePrice.setBounds(400, 80, 200, 20);

        btnCreateConfirm = new JButton("Confirm");
        btnCreateConfirm.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        btnCreateConfirm.setBackground(Color.WHITE);
        btnCreateConfirm.setBounds(600,120,115, 40);

        labelCreateCompMainText = new JLabel("Introduceti numele si produsele dorite:");
        labelCreateCompMainText.setFont(new Font("Times New Roman", Font.PLAIN, 15));
        labelCreateCompMainText.setBounds(350, 0, 300, 40);

        btnCreateCompConfirm = new JButton("Confirm");
        btnCreateCompConfirm.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        btnCreateCompConfirm.setBackground(Color.WHITE);
        btnCreateCompConfirm.setBounds(600,120,115, 40);

        labelCreateCompName = new JLabel("Name:");
        labelCreateCompName.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        labelCreateCompName.setBounds(350, 50, 250, 20);

        labelCreateCompPrice = new JLabel("Produse:");
        labelCreateCompPrice.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        labelCreateCompPrice.setBounds(350, 80, 250, 20);

        textFieldCreateCompName = new JTextField();
        textFieldCreateCompName.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        textFieldCreateCompName.setBounds(400, 50, 200, 20);

        comboCreteCompProduse = new JComboBox();
        comboCreteCompProduse.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        comboCreteCompProduse.setBounds(400, 80, 200, 20);

        btnCreateCompAdd= new JButton("Add");
        btnCreateCompAdd.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        btnCreateCompAdd.setBackground(Color.WHITE);
        btnCreateCompAdd.setBounds(600,60,115, 40);

        textAreaCreateComp = new JTextArea("Current composite: ");
        textAreaCreateComp.setBackground(Color.WHITE);
        textAreaCreateComp.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        textAreaCreateComp.setBounds(300, 150, 200, 200);

        labelDeleteName = new JLabel("Name:");
        labelDeleteName.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        labelDeleteName.setBounds(350, 50, 250, 20);

        comboBoxDeleteItem = new JComboBox();
        comboBoxDeleteItem.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        comboBoxDeleteItem.setBounds(400, 50, 250, 20);

        btnDeleteConfirm = new JButton("Confirm");
        btnDeleteConfirm.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        btnDeleteConfirm.setBackground(Color.WHITE);
        btnDeleteConfirm.setBounds(600,120,115, 40);


        labelDeleteText = new JLabel("Alegeti produsul pe care doriti sa il stergeti");
        labelDeleteText.setFont(new Font("Times New Roman", Font.PLAIN, 15));
        labelDeleteText.setBounds(350, 0, 300, 40);


        checkEditBase = new JCheckBox("Base Product");
        checkEditBase.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        checkEditBase.setBackground(Color.WHITE);
        checkEditBase.setBounds(200, 30, 115, 40);

        checkEditComposite = new JCheckBox("Composite Product");
        checkEditComposite.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        checkEditComposite.setBackground(Color.WHITE);
        checkEditComposite.setBounds(200, 80, 115, 40);


        labelEditBaseMainText = new JLabel("Introduceti numele si pretul pentru produsul dorit:");
        labelEditBaseMainText.setFont(new Font("Times New Roman", Font.PLAIN, 15));
        labelEditBaseMainText.setBounds(350, 0, 300, 40);

        labelEditBaseName = new JLabel("Name:");
        labelEditBaseName.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        labelEditBaseName.setBounds(350, 80, 250, 20);

        textFieldEditBaseName = new JTextField();
        textFieldEditBaseName.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        textFieldEditBaseName.setBounds(400, 80, 200, 20);


        labelEditBasePrice = new JLabel("Price:");
        labelEditBasePrice.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        labelEditBasePrice.setBounds(350, 110, 250, 20);

        textFieldEditBasePrice = new JTextField();
        textFieldEditBasePrice.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        textFieldEditBasePrice.setBounds(400, 110, 200, 20);

        labelEditBaseCurrent = new JLabel("Current: ");
        labelEditBaseCurrent.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        labelEditBaseCurrent.setBounds(350, 50, 250, 20);

        comboBoxEditBaseCombo = new JComboBox();
        comboBoxEditBaseCombo.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        comboBoxEditBaseCombo.setBounds(400, 50, 250, 20);

        btnEditBaseConfirm = new JButton("Confirm");
        btnEditBaseConfirm.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        btnEditBaseConfirm.setBackground(Color.WHITE);
        btnEditBaseConfirm.setBounds(630,120,115, 40);

        comboBoxEditCompCombo = new JComboBox();
        comboBoxEditCompCombo.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        comboBoxEditCompCombo.setBounds(400, 50, 250, 20);

        btnEditCompConfirm = new JButton("Confirm");
        btnEditCompConfirm.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        btnEditCompConfirm.setBackground(Color.WHITE);
        btnEditCompConfirm.setBounds(630,160,115, 40);

        labelEditCompMainText = new JLabel("Introduceti numele si pretul pentru produsul dorit:");
        labelEditCompMainText.setFont(new Font("Times New Roman", Font.PLAIN, 15));
        labelEditCompMainText.setBounds(350, 0, 300, 40);

        labelEditCompName = new JLabel("Name:");
        labelEditCompName.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        labelEditCompName.setBounds(350, 80, 250, 20);


        textFieldEditCompName = new JTextField();
        textFieldEditCompName.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        textFieldEditCompName.setBounds(400, 80, 200, 20);

        labelEditCompCurrent = new JLabel("Current: ");
        labelEditCompCurrent.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        labelEditCompCurrent.setBounds(350, 50, 250, 20);

        btnEditCompAdd = new JButton("Add");
        btnEditCompAdd.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        btnEditCompAdd.setBackground(Color.WHITE);
        btnEditCompAdd.setBounds(630,100,115, 40);

        comboBoxEditCompNew = new JComboBox();
        comboBoxEditCompNew.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        comboBoxEditCompNew.setBackground(Color.WHITE);
        comboBoxEditCompNew.setBounds(400,110,200, 20);

        textAreaEditComp = new JTextArea("Current composite: ");
        textAreaEditComp.setBackground(Color.WHITE);
        textAreaEditComp.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        textAreaEditComp.setBounds(300, 150, 200, 300);

        menuTable = new JTable();

        menuScrollPane = new JScrollPane(menuTable);
        menuScrollPane.setBounds(200, 50, 400, 400);
        frame.setVisible(true);
    }

    public JFrame getFrame() {
        return frame;
    }

    public JTextField getTextFieldCreateCompName() {
        return textFieldCreateCompName;
    }

    public ArrayList<MenuItem> getCompCurrentList() {
        return compCurrentList;
    }

    public void addListenerCreate(ActionListener actionListener){
        btnCreate.addActionListener(actionListener);
    }

    public void addListenerEdit(ActionListener actionListener){
        btnEdit.addActionListener(actionListener);
    }

    public void addListenerDelete(ActionListener actionListener){
        btnDelete.addActionListener(actionListener);
    }

    public void addListenerCreateBase(ActionListener actionListener){
        checkCreateBase.addActionListener(actionListener);
    }

    public String getTextFieldCreateBaseName() {
        return textFieldCreateBaseName.getText();
    }

    public void setTextFieldCreateBaseName(String textFieldCreateBaseName) {
        this.textFieldCreateBaseName.setText(textFieldCreateBaseName);
    }

    public void setTextFieldCreateBasePrice(String textFieldCreateBasePrice) {
        this.textFieldCreateBasePrice.setText(textFieldCreateBasePrice);
    }

    public String getTextFieldCreateBasePrice() {
        return textFieldCreateBasePrice.getText();
    }

    public void addListenerCreateComposite(ActionListener actionListener){
        checkComoposite.addActionListener(actionListener);
    }

    public void addListenerAddBase(ActionListener actionListener){
        this.btnCreateConfirm.addActionListener(actionListener);
    }

    public void addListenerAddComp(ActionListener actionListener){
        this.btnCreateCompAdd.addActionListener(actionListener);
    }

    public void addListenerConfirmComp(ActionListener actionListener){
        this.btnCreateCompConfirm.addActionListener(actionListener);
    }

    public void addListenerBtnDeleteConfirm(ActionListener actionListener){
        this.btnDeleteConfirm.addActionListener(actionListener);
    }

    public void addListenerCheckBaseEdit(ActionListener actionListener){
        this.checkEditBase.addActionListener(actionListener);
    }

    public void addListenerCheckCompEdit(ActionListener actionListener){
        this.checkEditComposite.addActionListener(actionListener);
    }

    public void addListenerEditBaseConfirm(ActionListener actionListener){
        this.btnEditBaseConfirm.addActionListener(actionListener);
    }

    public void addListenerEditCompAdd(ActionListener actionListener){
        this.btnEditCompAdd.addActionListener(actionListener);
    }

    public void addListenerEditCompConfirm(ActionListener actionListener){
        this.btnEditCompConfirm.addActionListener(actionListener);
    }

    public void addListenerMenu(ActionListener actionListener){
        this.btnMenu.addActionListener(actionListener);
    }

    public void showBase() {
        frame.getContentPane().add(textFieldCreateBasePrice);
        frame.getContentPane().add(btnCreateConfirm);
        frame.getContentPane().add(textFieldCreateBaseName);
        frame.getContentPane().add(labelCreateBaseName);
        frame.getContentPane().add(labelCreateBaseMainText);
        frame.getContentPane().add(labelCreateBasePrice);
        frame.revalidate();
        frame.repaint();
    }

    public void hideBase(){
        frame.getContentPane().remove(textFieldCreateBasePrice);
        frame.getContentPane().remove(btnCreateConfirm);
        frame.getContentPane().remove(textFieldCreateBaseName);
        frame.getContentPane().remove(labelCreateBaseName);
        frame.getContentPane().remove(labelCreateBaseMainText);
        frame.getContentPane().remove(labelCreateBasePrice);
        textFieldCreateBaseName.setText("");
        textFieldCreateBasePrice.setText("");
        frame.revalidate();
        frame.repaint();
    }

    public void showComposite(){
        frame.getContentPane().add(labelCreateCompMainText);
        frame.getContentPane().add(btnCreateCompConfirm);
        frame.getContentPane().add(labelCreateCompName);
        frame.getContentPane().add(labelCreateCompPrice);
        frame.getContentPane().add(textFieldCreateCompName);
        frame.getContentPane().add(comboCreteCompProduse);
        frame.getContentPane().add(textAreaCreateComp);
        frame.getContentPane().add(btnCreateCompAdd);
        frame.revalidate();
        frame.repaint();
    }

    public void hideComposite(){
        frame.getContentPane().remove(labelCreateCompMainText);
        frame.getContentPane().remove(btnCreateCompConfirm);
        frame.getContentPane().remove(labelCreateCompName);
        frame.getContentPane().remove(labelCreateCompPrice);
        frame.getContentPane().remove(textFieldCreateCompName);
        frame.getContentPane().remove(comboCreteCompProduse);
        frame.getContentPane().remove(textAreaCreateComp);
        frame.getContentPane().remove(btnCreateCompAdd);
        frame.revalidate();
        frame.repaint();
    }

    public void showCheck(){
        frame.getContentPane().add(checkCreateBase);
        frame.getContentPane().add(checkComoposite);
        frame.revalidate();
        frame.repaint();
    }

    public void hideCheck(){
        frame.getContentPane().remove(checkCreateBase);
        frame.getContentPane().remove(checkComoposite);
        checkComoposite.setSelected(false);
        checkCreateBase.setSelected(false);
        frame.revalidate();
        frame.repaint();
    }

    public void showDelete(){
        frame.getContentPane().add(comboBoxDeleteItem);
        frame.getContentPane().add(labelDeleteName);
        frame.getContentPane().add(btnDeleteConfirm);
        frame.getContentPane().add(labelDeleteText);
        frame.revalidate();
        frame.repaint();
    }

    public void hideDelete(){
        frame.getContentPane().remove(comboBoxDeleteItem);
        frame.getContentPane().remove(labelDeleteName);
        frame.getContentPane().remove(btnDeleteConfirm);
        frame.getContentPane().remove(labelDeleteText);

        frame.revalidate();
        frame.repaint();
    }

    public void showCheckEdit(){
        frame.getContentPane().add(checkEditBase);
        frame.getContentPane().add(checkEditComposite);
        checkEditBase.setSelected(false);
        checkEditComposite.setSelected(false);
        frame.revalidate();
        frame.repaint();
    }

    public void hideCheckEdit(){
        frame.getContentPane().remove(checkEditBase);
        frame.getContentPane().remove(checkEditComposite);
        checkEditBase.setSelected(false);
        checkEditComposite.setSelected(false);

        frame.revalidate();
        frame.repaint();

    }

    public void showBaseEdit(){
        frame.getContentPane().add(labelEditBaseMainText);
        frame.getContentPane().add(labelEditBaseName);
        frame.getContentPane().add(labelEditBasePrice);
        frame.getContentPane().add(textFieldEditBaseName);
        frame.getContentPane().add(textFieldEditBasePrice);
        frame.getContentPane().add(comboBoxEditBaseCombo);
        frame.getContentPane().add(labelEditBaseCurrent);
        frame.getContentPane().add(btnEditBaseConfirm);
        frame.revalidate();
        frame.repaint();
    }

    public void hideBaseEdit(){
        frame.getContentPane().remove(labelEditBaseMainText);
        frame.getContentPane().remove(labelEditBaseName);
        frame.getContentPane().remove(labelEditBasePrice);
        frame.getContentPane().remove(textFieldEditBaseName);
        frame.getContentPane().remove(textFieldEditBasePrice);
        frame.getContentPane().remove(comboBoxEditBaseCombo);
        frame.getContentPane().remove(labelEditBaseCurrent);
        frame.getContentPane().remove(btnEditBaseConfirm);

        frame.revalidate();
        frame.repaint();
    }

    public void showCompEdit(){
        frame.getContentPane().add(comboBoxEditCompCombo);
        frame.getContentPane().add(btnEditCompConfirm);
        frame.getContentPane().add(labelEditCompMainText);
        frame.getContentPane().add(labelEditCompName);
        frame.getContentPane().add(textFieldEditCompName);
        frame.getContentPane().add(labelEditCompCurrent);
        frame.getContentPane().add(btnEditCompAdd);
        frame.getContentPane().add(comboBoxEditCompNew);
        frame.getContentPane().add(textAreaEditComp);
        frame.revalidate();
        frame.repaint();
    }

    public void hideCompEdit(){
        frame.getContentPane().remove(comboBoxEditCompCombo);
        frame.getContentPane().remove(btnEditCompConfirm);
        frame.getContentPane().remove(labelEditCompMainText);
        frame.getContentPane().remove(labelEditCompName);
        frame.getContentPane().remove(textFieldEditCompName);
        frame.getContentPane().remove(labelEditCompCurrent);
        frame.getContentPane().remove(btnEditCompAdd);
        frame.getContentPane().remove(comboBoxEditCompNew);
        frame.getContentPane().remove(textAreaEditComp);
        frame.revalidate();
        frame.repaint();
    }

    public void showMenu(){
        //frame.getContentPane().add(menuTable);
        //menuScrollPane.setViewportView(menuTable);
        frame.getContentPane().add(menuScrollPane);

        frame.revalidate();
        frame.repaint();
    }

    public void hideMenu(){
        frame.getContentPane().remove(menuScrollPane);
        frame.revalidate();
        frame.repaint();
    }

    public JCheckBox getCheckComoposite() {
        return checkComoposite;
    }

    public JComboBox getComboCreteCompProduse() {
        return comboCreteCompProduse;
    }

    public JComboBox getComboBoxDeleteItem() {
        return comboBoxDeleteItem;
    }

    public JComboBox getComboBoxEditCompCombo() {
        return comboBoxEditCompCombo;
    }

    public JComboBox getComboBoxEditBaseCombo() {
        return comboBoxEditBaseCombo;
    }

    public JCheckBox getCheckEditBase() {
        return checkEditBase;
    }

    public JComboBox getComboBoxEditCompNew() {
        return comboBoxEditCompNew;
    }

    public JCheckBox getCheckEditComposite() {
        return checkEditComposite;
    }

    public void setCompEditCurrentList(ArrayList<MenuItem> compEditCurrentList) {
        this.compEditCurrentList = compEditCurrentList;
    }

    public JCheckBox getCheckCreateBase() {
        return checkCreateBase;
    }

    public JTextArea getTextAreaEditComp() {
        return textAreaEditComp;
    }

    public ArrayList<MenuItem> getCompEditCurrentList() {
        return compEditCurrentList;
    }

    public JTextField getTextFieldEditCompName() {
        return textFieldEditCompName;
    }

    public JTextField getTextFieldEditBaseName() {
        return textFieldEditBaseName;
    }

    public JTextField getTextFieldEditBasePrice() {
        return textFieldEditBasePrice;
    }

    public JTable getMenuTable() {
        return menuTable;
    }

    public JTextArea getTextAreaCreateComp() {
        return textAreaCreateComp;
    }
}
