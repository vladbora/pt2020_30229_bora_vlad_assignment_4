package presentation;

import java.awt.*;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;

public class StartingGUI{

    private JButton btnAdmin;
    private JButton btnWaiter;
    private JButton btnChef;

    public StartingGUI()
    {
        this.initialize();
    }
    private void initialize() {
        JFrame frame = new JFrame();
        frame.getContentPane().setBackground(Color.WHITE);
        frame.setBounds(100, 100, 350, 400);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);
        frame.setTitle("Starting page");

        btnAdmin = new JButton("Admin");
        btnAdmin.setBackground(Color.WHITE);
        btnAdmin.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        btnAdmin.setBounds(100, 50, 130, 40);
        frame.getContentPane().add(btnAdmin);

        btnWaiter = new JButton("Waiter");
        btnWaiter.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        btnWaiter.setBackground(Color.WHITE);
        btnWaiter.setBounds(100, 150, 130, 40);
        frame.getContentPane().add(btnWaiter);


        btnChef = new JButton("Chef");
        btnChef.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        btnChef.setBackground(Color.WHITE);
        btnChef.setBounds(100, 250, 130, 40);
        frame.getContentPane().add(btnChef);

        frame.setVisible(true);
    }

    public void addListenerAdmin(ActionListener actionListener){
        btnAdmin.addActionListener(actionListener);
    }
    public void addListenerWaiter(ActionListener actionListener){
        btnWaiter.addActionListener(actionListener);
    }
    public void addListenerChef(ActionListener actionListener){
        btnChef.addActionListener(actionListener);
    }
}
