package presentation;

import business.MenuItem;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class WaiterGUI {

    private JFrame frame;

    private JButton btnOrder;
    private JButton btnCompute;
    private JComboBox comboBoxOrderItem;

    private JLabel labelOrderName;
    private JLabel labelOrderText;
    private JLabel labelCompute;
    private JLabel labelComputeRezult;
    private JLabel labelComputeText;
    private JLabel labelOrderMasa;

    private JButton btnOrderConfirm;
    private JButton btnOrderAdd;
    private JButton btnComputePrice;
    private JButton btnBill;
    private JButton btnShowOrder;


    private JTextArea textAreaOrder;

    private JTextField textFieldComputeTable;
    private JTextField textFieldComputeRezult;
    private JTextField textFieldOrder;
    private JTable ordersTable;
    private JScrollPane ordersScrollPane;

    private ArrayList<business.MenuItem> addToOrder = new ArrayList<MenuItem>();

    public WaiterGUI() {
        frame = new JFrame();
        frame.getContentPane().setBackground(Color.WHITE);
        frame.setBounds(400, 200, 850, 500);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.getContentPane().setLayout(null);
        frame.setTitle("Waiter Page");

        btnOrder = new JButton("New Order");
        btnOrder.setBackground(Color.WHITE);
        btnOrder.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        btnOrder.setBounds(50, 50, 130, 40);
        frame.getContentPane().add(btnOrder);

        btnCompute = new JButton("Get price");
        btnCompute.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        btnCompute.setBackground(Color.WHITE);
        btnCompute.setBounds(50, 125, 130, 40);
        frame.getContentPane().add(btnCompute);

        btnShowOrder= new JButton("Show orders");
        btnShowOrder.setBackground(Color.WHITE);
        btnShowOrder.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        btnShowOrder.setBounds(50, 200, 130, 40);
        frame.getContentPane().add(btnShowOrder);

        btnBill = new JButton("Generate Bill");
        btnBill.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        btnBill.setBackground(Color.WHITE);
        btnBill.setBounds(600, 250, 130, 40);

        comboBoxOrderItem = new JComboBox();
        comboBoxOrderItem.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        comboBoxOrderItem.setBounds(400, 50, 250, 20);

        labelOrderName = new JLabel("Name:");
        labelOrderName.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        labelOrderName.setBounds(350, 50, 250, 20);

        labelOrderText = new JLabel("Alegeti ce doriti sa comandati");
        labelOrderText.setFont(new Font("Times New Roman", Font.PLAIN, 15));
        labelOrderText.setBounds(350, 0, 300, 40);

        textFieldOrder = new JTextField();
        textFieldOrder.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        textFieldOrder.setBounds(400, 90, 250, 20);


        labelOrderMasa = new JLabel("Masa:");
        labelOrderMasa.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        labelOrderMasa.setBounds(350, 90, 250, 20);

        btnOrderConfirm = new JButton("Confirm");
        btnOrderConfirm.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        btnOrderConfirm.setBackground(Color.WHITE);
        btnOrderConfirm.setBounds(600,120,115, 40);

        btnOrderAdd = new JButton("Add");
        btnOrderAdd.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        btnOrderAdd.setBackground(Color.WHITE);
        btnOrderAdd.setBounds(700,50,115, 40);

        textAreaOrder = new JTextArea("Menu items");
        textAreaOrder.setBackground(Color.WHITE);
        textAreaOrder.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        textAreaOrder.setBounds(300, 150, 200, 200);
        textAreaOrder.setEditable(false);


        btnComputePrice= new JButton("Compute price");
        btnComputePrice.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        btnComputePrice.setBackground(Color.WHITE);
        btnComputePrice.setBounds(600,150,130, 40);

        textFieldComputeTable = new JTextField();
        textFieldComputeTable.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        textFieldComputeTable.setBounds(400, 50, 250, 20);

        labelCompute = new JLabel("Masa:");
        labelCompute.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        labelCompute.setBounds(350, 50, 250, 20);

        textFieldComputeRezult = new JTextField();
        textFieldComputeRezult.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        textFieldComputeRezult.setBounds(400, 100, 250, 20);
        textFieldComputeRezult.setEditable(false);

        labelComputeRezult = new JLabel("Rezultat:");
        labelComputeRezult.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        labelComputeRezult.setBounds(350, 100, 250, 20);

        labelComputeText = new JLabel("Alegeti daca doreti sa aflati pretul sau nota:");
        labelComputeText.setFont(new Font("Times New Roman", Font.PLAIN, 15));
        labelComputeText.setBounds(350, 0, 300, 40);


        ordersTable = new JTable();

        ordersScrollPane = new JScrollPane(ordersTable);
        ordersScrollPane.setBounds(200, 50, 400, 400);

        frame.setVisible(true);
    }

    public void addListenerOrder(ActionListener actionListener){
        btnOrder.addActionListener(actionListener);
    }

    public void addListenerBill(ActionListener actionListener){
        btnBill.addActionListener(actionListener);
    }

    public void addListenerCompute(ActionListener actionListener){
        btnCompute.addActionListener(actionListener);
    }

    public void confirmOrderListener(ActionListener actionListener){
        btnOrderConfirm.addActionListener(actionListener);
    }

    public JTextField getTextFieldComputeTable() {
        return textFieldComputeTable;
    }

    public JTextField getTextFieldComputeRezult() {
        return textFieldComputeRezult;
    }

    public JTextField getTextFieldOrder() {
        return textFieldOrder;
    }

    public void addListenerAddOrder(ActionListener actionListener){
        this.btnOrderAdd.addActionListener(actionListener);
    }

    public void computePriceListener(ActionListener actionListener){
        this.btnComputePrice.addActionListener(actionListener);
    }

    public void tableListener(ActionListener actionListener){
        this.btnShowOrder.addActionListener(actionListener);
    }

    public ArrayList<MenuItem> getAddToOrder() {
        return addToOrder;
    }

    public JTextArea getTextAreaOrder() {
        return textAreaOrder;
    }

    public void showOrder(){
        frame.getContentPane().add(comboBoxOrderItem);
        frame.getContentPane().add(labelOrderName);
        frame.getContentPane().add(labelOrderText);
        frame.getContentPane().add(btnOrderConfirm);
        frame.getContentPane().add(btnOrderAdd);
        frame.getContentPane().add(textAreaOrder);
        frame.getContentPane().add(textFieldOrder);
        frame.getContentPane().add(labelOrderMasa);
        frame.revalidate();
        frame.repaint();
    }

    public void hideOrder(){
        frame.getContentPane().remove(comboBoxOrderItem);
        frame.getContentPane().remove(labelOrderName);
        frame.getContentPane().remove(labelOrderText);
        frame.getContentPane().remove(btnOrderConfirm);
        frame.getContentPane().remove(btnOrderAdd);
        frame.getContentPane().remove(textAreaOrder);
        frame.getContentPane().remove(textFieldOrder);
        frame.getContentPane().remove(labelOrderMasa);
        frame.revalidate();
        frame.repaint();
    }

    public void showCompute(){
        frame.getContentPane().add(btnComputePrice);
        frame.getContentPane().add(textFieldComputeTable);
        frame.getContentPane().add(labelCompute);
        frame.getContentPane().add(textFieldComputeRezult);
        frame.getContentPane().add(labelComputeRezult);
        frame.getContentPane().add(labelComputeText);
        frame.getContentPane().add(btnBill);
        frame.revalidate();
        frame.repaint();
    }

    public void hideCompute(){
        frame.getContentPane().remove(btnComputePrice);
        frame.getContentPane().remove(textFieldComputeTable);
        frame.getContentPane().remove(labelCompute);
        frame.getContentPane().remove(textFieldComputeRezult);
        frame.getContentPane().remove(labelComputeRezult);
        frame.getContentPane().remove(labelComputeText);
        frame.getContentPane().remove(btnBill);
        textFieldComputeRezult.setText("");
        textFieldComputeRezult.setText("");
        frame.revalidate();
        frame.repaint();
    }

    public  void showTable(){
        frame.getContentPane().add(ordersScrollPane);
        frame.revalidate();
        frame.repaint();
    }

    public  void hideTable(){
        frame.getContentPane().remove(ordersScrollPane);
        frame.revalidate();
        frame.repaint();
    }

    public void setAddToOrder(ArrayList<MenuItem> addToOrder) {
        this.addToOrder = addToOrder;
    }

    public JTable getOrdersTable() {
        return ordersTable;
    }

    public JComboBox getComboBoxOrderItem() {
        return comboBoxOrderItem;
    }
}
