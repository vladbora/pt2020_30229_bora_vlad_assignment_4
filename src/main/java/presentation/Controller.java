package presentation;

import business.*;
import data.FileWriter;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Vector;

public class Controller {
    private StartingGUI startingGUI;
    private Restaurant restaurant;
    private AdminGUI adminGUI;
    private WaiterGUI waiterGUI;
    private ChefGUI chefGUI;
    private String fileName;

    public Controller(StartingGUI startingGUI, String fileName) {
        this.startingGUI = startingGUI;
        this.fileName = fileName;
        startingGUI.addListenerAdmin(new AdminListener());
        startingGUI.addListenerChef(new ChefListener());
        startingGUI.addListenerWaiter(new WaiterListener());
        restaurant = new Restaurant(fileName);
    }

    class AdminListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            adminGUI = new AdminGUI();
            adminGUI.addListenerCreateBase(new AdminBaseCheckListener());
            adminGUI.addListenerCreateComposite(new AdminCompositeCheckListener());
            adminGUI.addListenerCreate(new CreareListener());
            adminGUI.addListenerDelete(new DeleteListener());
            adminGUI.addListenerAddBase(new AddBaseProductListener());
            adminGUI.addListenerAddComp(new AddToCurrentComposite());
            adminGUI.addListenerConfirmComp(new AddCompositeToMenu());
            adminGUI.addListenerBtnDeleteConfirm(new DeleteItemListener());
            adminGUI.addListenerEdit(new EditListener());
            adminGUI.addListenerCheckBaseEdit(new CheckEditBaseListener());
            adminGUI.addListenerCheckCompEdit(new CheckEditCompListener());
            adminGUI.addListenerEditBaseConfirm(new EditBaseConfirmListener());
            adminGUI.addListenerEditCompAdd(new EditAddItemToOrderListener());
            adminGUI.addListenerEditCompConfirm(new EditCompConfirmListener());
            adminGUI.addListenerMenu(new MenuListener());
            repopulateAdminComboBox();
        }
    }

    class WaiterListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            waiterGUI = new WaiterGUI();
            waiterGUI.addListenerOrder(new OrderListener());
            waiterGUI.addListenerCompute(new ComputeListener());
            waiterGUI.addListenerAddOrder(new AddItemToOrderListener());
            waiterGUI.confirmOrderListener(new ConfirmOrderListener());
            waiterGUI.addListenerBill(new GenerateBillListener());
            waiterGUI.computePriceListener(new ComputePriceListener());
            waiterGUI.tableListener(new TableListener());
            repopulateWaiterComboBox();
        }
    }

    class ChefListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            chefGUI = new ChefGUI();
            restaurant.setObserver(chefGUI);
        }
    }

    class AdminBaseCheckListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if(adminGUI.getCheckCreateBase().isSelected()) {
                adminGUI.getCheckComoposite().setSelected(false);
                adminGUI.showBase();
                adminGUI.hideComposite();
            }
            else
            {
                adminGUI.hideBase();
            }
        }
    }

    class AdminCompositeCheckListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if(adminGUI.getCheckComoposite().isSelected()) {
                adminGUI.getCheckCreateBase().setSelected(false);
                adminGUI.hideBase();
                adminGUI.showComposite();
            }
            else
            {
                adminGUI.hideComposite();
            }
        }
    }

    class EditListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            adminGUI.showCheckEdit();
            adminGUI.hideCheck();
            adminGUI.hideDelete();
            adminGUI.hideBase();
            adminGUI.hideComposite();
            adminGUI.hideMenu();
        }
    }

    class CreareListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            adminGUI.showCheck();
            adminGUI.hideDelete();
            adminGUI.hideBaseEdit();
            adminGUI.hideCheckEdit();
            adminGUI.hideCompEdit();
        }
    }

    class DeleteListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            adminGUI.showDelete();
            adminGUI.hideComposite();
            adminGUI.hideBase();
            adminGUI.hideCheck();
            adminGUI.hideBaseEdit();
            adminGUI.hideCheckEdit();
            adminGUI.hideCompEdit();
            adminGUI.hideMenu();
        }
    }

    class OrderListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            waiterGUI.showOrder();
            waiterGUI.hideCompute();
            waiterGUI.hideTable();
        }
    }

    class ComputeListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            waiterGUI.showCompute();
            waiterGUI.hideOrder();
            waiterGUI.hideTable();
        }
    }

    class TableListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            waiterGUI.hideCompute();
            waiterGUI.hideOrder();
            fillWaiterTable();
            waiterGUI.showTable();
        }
    }

    private void fillWaiterTable(){
        JTable table = waiterGUI.getOrdersTable();
        table.removeAll();
        Vector<Vector<Object>> rows = new Vector<Vector<Object>>();
        Vector<String> name = new Vector<>();
        name.add("ID");
        name.add("Table");
        name.add("Date");

        for(Order m: restaurant.getOrders()) {
            Vector<Object> row = new Vector<>();
            row.add(m.getOrderID());
            row.add(m.getTable());
            row.add(m.getDate());
            rows.add(row);
        }

        table.setModel(new DefaultTableModel(rows, name));
    }

    class AddBaseProductListener implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            restaurant.createItem(adminGUI.getTextFieldCreateBaseName(), Integer.parseInt(adminGUI.getTextFieldCreateBasePrice()));
            FileWriter.write(restaurant.getMenuItems(), fileName);
            repopulateAdminComboBox();
            adminGUI.setTextFieldCreateBaseName("");
            adminGUI.setTextFieldCreateBasePrice("");
        }
    }

    class AddToCurrentComposite implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            String itemName = (String)adminGUI.getComboCreteCompProduse().getSelectedItem();
            for (MenuItem menuItem : restaurant.getMenuItems())
            {
                if (itemName.equals(menuItem.getName()))
                {
                    adminGUI.getCompCurrentList().add(menuItem);
                    break;
                }
            }
            adminGUI.getComboCreteCompProduse().removeItem(adminGUI.getComboCreteCompProduse().getSelectedItem());
            adminGUI.getTextAreaCreateComp().setText(adminGUI.getTextAreaCreateComp().getText() + " " + itemName);
        }
    }

    class AddCompositeToMenu implements ActionListener{
        public  void actionPerformed(ActionEvent e){
            restaurant.createItem(adminGUI.getTextFieldCreateCompName().getText(), adminGUI.getCompCurrentList());
            adminGUI.getCompCurrentList().clear();
            repopulateAdminComboBox();
            FileWriter.write(restaurant.getMenuItems(), fileName);
            adminGUI.getTextAreaCreateComp().setText("Current composite:");
            adminGUI.getTextFieldCreateCompName().setText("");
        }
    }

    class DeleteItemListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            int index = 0;
            for (MenuItem menuItem : restaurant.getMenuItems())
            {
                if (menuItem.getName().equals((String) adminGUI.getComboBoxDeleteItem().getSelectedItem()))
                {
                    index = restaurant.getMenuItems().indexOf(menuItem);
                }
            }
            restaurant.deleteItem(index);
            repopulateAdminComboBox();
            FileWriter.write(restaurant.getMenuItems(), fileName);
        }
    }

    class AddItemToOrderListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String itemName = (String)waiterGUI.getComboBoxOrderItem().getSelectedItem();
            for (MenuItem menuItem : restaurant.getMenuItems())
            {
                if (itemName.equals(menuItem.getName()))
                {
                    waiterGUI.getAddToOrder().add(menuItem);
                    break;
                }
            }
            waiterGUI.getComboBoxOrderItem().removeItem(waiterGUI.getComboBoxOrderItem().getSelectedItem());
            waiterGUI.getTextAreaOrder().setText(waiterGUI.getTextAreaOrder().getText() + " " + itemName);

        }
    }

    class ConfirmOrderListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            restaurant.createOrder(Integer.parseInt(waiterGUI.getTextFieldOrder().getText()), waiterGUI.getAddToOrder());
            waiterGUI.setAddToOrder(new ArrayList<MenuItem>());
            repopulateWaiterComboBox();
            waiterGUI.getTextFieldOrder().setText("");
            waiterGUI.getTextAreaOrder().setText("Comanda curenta: ");
        }
    }

    class GenerateBillListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            restaurant.createBill(Integer.parseInt(waiterGUI.getTextFieldComputeTable().getText()));
        }
    }

    class ComputePriceListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            int rez = restaurant.computePrice(Integer.parseInt(waiterGUI.getTextFieldComputeTable().getText()));
            waiterGUI.getTextFieldComputeRezult().setText(Integer.toString(rez));
        }
    }

    class CheckEditBaseListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if(adminGUI.getCheckEditBase().isSelected()) {
                adminGUI.getCheckEditComposite().setSelected(false);
                adminGUI.showBaseEdit();
                adminGUI.hideCompEdit();
            }
            else
            {
                adminGUI.hideBaseEdit();
            }
        }
    }

    class CheckEditCompListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if(adminGUI.getCheckEditComposite().isSelected()) {
                adminGUI.getCheckEditBase().setSelected(false);
                adminGUI.hideBaseEdit();
                adminGUI.showCompEdit();
            }
            else
            {
                adminGUI.hideCompEdit();
            }
        }
    }

    class MenuListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            adminGUI.hideCheck();
            adminGUI.hideDelete();
            adminGUI.hideBaseEdit();
            adminGUI.hideCheckEdit();
            adminGUI.hideCompEdit();
            adminGUI.hideBase();
            adminGUI.hideComposite();
            fillTable();
            adminGUI.showMenu();
        }
    }

    private void fillTable(){
        JTable table = adminGUI.getMenuTable();
        table.removeAll();
        Vector<Vector<Object>> rows = new Vector<Vector<Object>>();
        Vector<String> name = new Vector<>();
        name.add("Name");
        name.add("Price");

        for(MenuItem m: restaurant.getMenuItems()) {
            Vector<Object> row = new Vector<>();
            row.add(m.getName());
            row.add(m.getPrice());
            rows.add(row);
        }

        table.setModel(new DefaultTableModel(rows, name));
    }

    class EditBaseConfirmListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            int index = -1;
            for(MenuItem menuItem : restaurant.getMenuItems()){
                if (menuItem.getName().equals((String) adminGUI.getComboBoxEditBaseCombo().getSelectedItem())){
                    index = restaurant.getMenuItems().indexOf(menuItem);
                    break;
                }
            }
            if (index == -1)
            {
                System.out.println("Eroare la selecatea de item");
                return;
            }
            BaseProduct newBaseProduct = new BaseProduct(adminGUI.getTextFieldEditBaseName().getText(), Integer.parseInt(adminGUI.getTextFieldEditBasePrice().getText()));
            restaurant.editItem(index, newBaseProduct);
            adminGUI.getTextFieldEditBaseName().setText("");
            adminGUI.getTextFieldEditBasePrice().setText("");
            FileWriter.write(restaurant.getMenuItems(), fileName);
            repopulateAdminComboBox();
        }
    }

    class EditAddItemToOrderListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
           String itemName = (String)adminGUI.getComboBoxEditCompNew().getSelectedItem();
            for (MenuItem menuItem : restaurant.getMenuItems())
            {
                if (itemName.equals(menuItem.getName()))
                {
                    adminGUI.getCompEditCurrentList().add(menuItem);
                    break;
                }
            }
            adminGUI.getComboBoxEditCompNew().removeItem(adminGUI.getComboBoxEditCompNew().getSelectedItem());
            adminGUI.getTextAreaEditComp().setText(adminGUI.getTextAreaEditComp().getText() + "\n" + itemName);
        }
    }

    class EditCompConfirmListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            int index = -1;
            for(MenuItem menuItem : restaurant.getMenuItems()){
                if (menuItem.getName().equals((String) adminGUI.getComboBoxEditCompCombo().getSelectedItem())){
                    index = restaurant.getMenuItems().indexOf(menuItem);
                    break;
                }
            }
            if (index == -1)
            {
                System.out.println("Eroare la selecatea de item");
                return;
            }
            CompositeProduct newCompProduct = new CompositeProduct((String) adminGUI.getTextFieldEditCompName().getText());
            for(MenuItem menuItem : adminGUI.getCompEditCurrentList())
            {
                newCompProduct.addItem(menuItem);
            }
            System.out.println(index + " " + newCompProduct.getName());
            restaurant.editItem(index, newCompProduct);
            adminGUI.getTextFieldEditCompName().setText("");
            adminGUI.setCompEditCurrentList(new ArrayList<MenuItem>());
            FileWriter.write(restaurant.getMenuItems(), fileName);
            repopulateAdminComboBox();
        }
    }

    private void repopulateAdminComboBox(){
        this.populateComboBox(adminGUI.getComboCreteCompProduse());
        this.populateComboBox(adminGUI.getComboBoxDeleteItem());
        this.populateComboBoxBase(adminGUI.getComboBoxEditBaseCombo());
        this.populateComboBoxComp(adminGUI.getComboBoxEditCompCombo());
        this.populateComboBox(adminGUI.getComboBoxEditCompNew());
    }

    private void repopulateWaiterComboBox(){
        this.populateComboBoxComp(waiterGUI.getComboBoxOrderItem());
    }

    private void populateComboBox(JComboBox comboBox){
        comboBox.removeAllItems();
        for (MenuItem menuItem : restaurant.getMenuItems()){
            comboBox.addItem(menuItem.getName());
        }
    }

    private void populateComboBoxBase(JComboBox comboBox) {
        comboBox.removeAllItems();
        for (MenuItem menuItem : restaurant.getMenuItems()){
            if (menuItem instanceof BaseProduct)
                comboBox.addItem(menuItem.getName());
        }
    }

    private void populateComboBoxComp(JComboBox comboBox) {
        comboBox.removeAllItems();
        for (MenuItem menuItem : restaurant.getMenuItems()){
            if (menuItem instanceof CompositeProduct)
                comboBox.addItem(menuItem.getName());
        }
    }

}
