package presentation;

import business.MenuItem;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class ChefGUI implements Observer  {


    private JFrame frame = new JFrame();
    private JTextArea textArea = new JTextArea();

    public ChefGUI(){
        this.initialize();
    }

    public void initialize(){
        frame.getContentPane().setBackground(Color.WHITE);
        frame.setBounds(300, 100, 850, 600);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.getContentPane().setLayout(null);
        frame.setTitle("Chef page");

        textArea.setBounds(50, 50,350 ,350);
        textArea.setFont(new Font("Times New Roman", Font.PLAIN, 12));
        textArea.setEditable(false);

        frame.getContentPane().add(textArea);
        frame.setVisible(true);
    }

    @Override
    public void update(Observable o, Object arg) {
        ArrayList<MenuItem> order = (ArrayList<MenuItem>)arg;
        textArea.setText("Comanda in lucru contine: ");
        for (MenuItem menuItem : order){
            textArea.setText(textArea.getText() + "\n" + menuItem.getName());
        }
    }

}
