package main;
import presentation.Controller;
import presentation.StartingGUI;

public class MainClass {

    public static void main(String[] args)
    {
        String fileName;
        if (args.length > 0)
        {
            fileName = args[0];
        }
        else {
            fileName = "restaurant.ser";
        }

        StartingGUI gui = new StartingGUI();
        Controller controller = new Controller(gui, fileName);
    }

}
