package data;

import business.MenuItem;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

public class RestaurantSerializator {

    public static ArrayList<MenuItem> loadMenu(String fileName){
        ArrayList<MenuItem> menu = new ArrayList<>();
        ObjectInputStream toRead = null;
        try {
            toRead = new ObjectInputStream(new FileInputStream(fileName));
            int size = toRead.readInt();
            for(int i = 0; i < size; ++i)
            {
                menu.add((MenuItem)toRead.readObject());
            }
            toRead.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return menu;
    }

}
