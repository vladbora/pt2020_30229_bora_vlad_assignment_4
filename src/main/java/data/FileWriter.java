package data;

import business.MenuItem;

import java.io.*;
import java.util.ArrayList;

public class FileWriter {

    static public void write(ArrayList<MenuItem> menuItems, String fileName){
        try {
            FileOutputStream menu = new FileOutputStream(fileName);
            ObjectOutputStream out = new ObjectOutputStream(menu);
            out.writeInt(menuItems.size());
            for(MenuItem current : menuItems)
                out.writeObject(current);
            menu.close();
            out.close();
        } catch (FileNotFoundException e) {
        } catch (IOException e) {
        }
    }

}
