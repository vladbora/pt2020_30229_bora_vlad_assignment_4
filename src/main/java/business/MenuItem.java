package business;

import java.io.Serializable;

public abstract class MenuItem  implements Serializable{

    private static final long serialVersionUID = 1L;
    private int price;
    private String name;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return this.price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
