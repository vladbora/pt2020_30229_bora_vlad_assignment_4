package business;

import java.util.ArrayList;

public class CompositeProduct extends MenuItem {
    private ArrayList<MenuItem> items = new ArrayList<MenuItem>();

    public CompositeProduct(){

    }

    public ArrayList<MenuItem> getItems() {
        return items;
    }

    public CompositeProduct(String name) {
        this.setName(name);
    }

    public void addItem(MenuItem menuItem){
        items.add(menuItem);
        this.setPrice(this.getPrice() + menuItem.getPrice());
    }


}
