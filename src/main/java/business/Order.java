package business;

import java.util.Date;
import java.util.Objects;

public class Order {

    private int OrderID, Table;
    private Date Date;

    public Order(int orderID, int table, java.util.Date date) {
        OrderID = orderID;
        Table = table;
        Date = date;
    }

    public void setOrderID(int orderID) {
        OrderID = orderID;
    }

    public void setTable(int table) {
        Table = table;
    }

    public void setDate(java.util.Date date) {
        Date = date;
    }

    public int getOrderID() {
        return OrderID;
    }

    public int getTable() {
        return Table;
    }

    public java.util.Date getDate() {
        return Date;
    }

    public int hashCode() {
        return Objects.hash(this.Date, this.Table, this.OrderID);
    }
}
