package business;

import java.util.ArrayList;

public interface IRestaurantProcessing {

    /**
     * a function that adds a BaseProduct to the menu
     * @pre  name != null && price > 0
     * @param name the name of BaseProduct that is going to be added to the menu
     * @param price the price of the BaseeProduct that is going to be added to the menu
     * @post getSize() == getSize()@pre + 1
     */
    public void createItem(String name, int price);

    /**
     * a function that adds a CompositeProduct to the menu
     * @pre name != null && components.getSize() > 0
     * @param name the name of the CompositeProduct
     * @param components the list of itemss of the composite product
     * @post  @post getSize() == getSize()@pre + 1
     */
    public void createItem(String name, ArrayList<MenuItem> components);

    /**
     * a function that delets a MenuItem from the menu
     * @pre id > 0 && id < getSize();
     * @param id the id of the item to be deleted
     * @post  @post getSize() == getSize()@pre - 1
     */
    public void deleteItem(int id);

    /**
     * a function that edits an item from the menu
     * @pre id > 0 && id < getSize() && replace != null
     * @param id the id of the MenuItem
     * @param replace the new MenuItem
     * @post  @post getSize() == getSize()@pre
     */
    public void editItem(int id, MenuItem replace);

    /**
     * a function that creats a new order
     * @pre table > 0 && order != null
     * @param table the table for the order
     * @param order the items that are going to be inserted into the order
     */
    public void createOrder(int table, ArrayList<MenuItem> order);

    /**
     * a function that generates a bill in txt format
     * @pre table > 0
     * @param table the table that the bill is for
     * @post getSize() == getSize()@pre - 1
     */
    public void createBill(int table);

    /**
     * a function that computes the price for a table
     * @pre table > 0
     * @param table the table that the price is for
     * @post @rezult > 0
     */
    public int computePrice(int table);
}
