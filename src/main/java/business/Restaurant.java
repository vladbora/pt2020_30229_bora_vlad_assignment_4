package business;

import data.RestaurantSerializator;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

public class Restaurant extends Observable implements IRestaurantProcessing{


    private HashMap<Order, ArrayList<MenuItem>> customerOrders = new HashMap<Order, ArrayList<MenuItem>>();
    /**
     *
     * @invariant isWellFormed();
     */
    private ArrayList<MenuItem> menuItems;
    private ArrayList<Order> orders = new ArrayList<Order>();
    private Observer observer;

    public Restaurant(String fileName){
        menuItems = new ArrayList<MenuItem>();
        menuItems = RestaurantSerializator.loadMenu(fileName);
    }

    @Override
    public void createItem(String name, int price) {
        assert name != null && price > 0 : "invalid parameters";
        assert isWellFormed() : "is not well formed";
        int value = menuItems.size();
        menuItems.add(new BaseProduct(name, price));
        assert menuItems.size() == value + 1 : "invalid size";
        assert isWellFormed() : "is not well formed";
    }

    @Override
    public void createItem(String name, ArrayList<MenuItem> components) {
        assert name != null && components.size()> 0 : "invalid parameters";
        assert isWellFormed() : "is not well formded";
        int value = menuItems.size();
        CompositeProduct compositeProduct = new CompositeProduct(name);
        for (MenuItem menuItem : components){
            compositeProduct.addItem(menuItem);
        }
        menuItems.add(compositeProduct);
        assert isWellFormed() : "is not well formed";
        assert menuItems.size() == value+ 1 : "invalid size";
    }

    @Override
    public void deleteItem(int id) {
        assert id > 0 && id < menuItems.size() : "invalid size";
        assert isWellFormed(): "is not well formed";
        int value = menuItems.size();
        MenuItem myItem = menuItems.get(id);
        Iterator<MenuItem> c = menuItems.iterator();
        ArrayList<Integer> auxList = new ArrayList<Integer>();
        while(c.hasNext()){
            MenuItem current = c.next();
            if (current instanceof CompositeProduct){
                CompositeProduct myComposite = (CompositeProduct) current;
                if (myComposite.getItems().contains(myItem))
                    auxList.add(menuItems.indexOf(myComposite));
            }
        }
        for(Integer current : auxList){
            deleteItem(current);
            value--;
        }
        menuItems.remove(id);
        assert isWellFormed() : "is not well formed";
        assert menuItems.size() == value - 1 : "invalid size";
    }

    @Override
    public void editItem(int id, MenuItem replace) {
        assert id > 0 && id < menuItems.size() && replace != null : "invalid parameters";
        assert isWellFormed() : "is not well formed";
        int value = menuItems.size();
        this.menuItems.get(id).setName(replace.getName());
        this.menuItems.get(id).setPrice(replace.getPrice());
        if (replace instanceof CompositeProduct)
        {
            ((CompositeProduct) this.menuItems.get(id)).getItems().clear();
            for (MenuItem menuItem : ((CompositeProduct)replace).getItems()){
                ((CompositeProduct) this.menuItems.get(id)).addItem(menuItem);
            }
        }
        assert isWellFormed() : "is not well formed";
        assert menuItems.size() == value : "invalid modify" ;
    }

    @Override
    public void createOrder(int table, ArrayList<MenuItem> order) {
        assert table > 0 && order != null : "invalid parameters";
        assert isWellFormed() : "is not well formed";
        Order o = new Order(orders.size(), table, new Date(System.currentTimeMillis()));
        orders.add(o);
        customerOrders.put(o, order);
        notifyObserver(this, order);
        assert isWellFormed() : "is not well formed";
    }

    @Override
    public int computePrice(int table) {
        assert table > 0 : "invalid parameters";
        assert isWellFormed() : "is not well formed";
        int suma = 0;
        for(Order currentOrder : orders){
            if (currentOrder.getTable() == table){
                ArrayList<MenuItem> currentMenuItems = customerOrders.get(currentOrder);
                for(MenuItem currentMenuItem : currentMenuItems){
                    suma += currentMenuItem.getPrice();
                }
            }
        }
        assert isWellFormed() : "is not well formed";
        assert suma > 0 : "invalid price";
        return suma;
    }

    /**
     * a function that notifys the chef when he has to prepare an order
     * @param observable this parameter will be set to this instance
     * @param order the current order that ce has to prepare
     */
    public void notifyObserver(Observable observable, ArrayList<MenuItem> order) {
        try{

            observer.update(observable, order);
        }
        catch (Exception e)
        {
            System.out.println("Interfata pentru chef ar trebui sa fie activa!");
        }
    }

    @Override
    public void createBill(int table) {
        try {
            //create a temporary file
            assert table > 0 : "invalid parameters";
            assert isWellFormed() : "is not well formed";
            int value = menuItems.size();
            String fileName = "Bill" + new SimpleDateFormat("HH mm ss").format(Calendar.getInstance().getTime()) + ".txt";
            String text = "Comenzile de la masa sunt:\n";
            int price = 0;
            ArrayList<Integer> list = new ArrayList<Integer>();
            for(Order currentOrder : orders){
                if (currentOrder.getTable() == table){
                    text += "Comanada cu numarul " + currentOrder.getOrderID() + " in data de" + currentOrder.getDate() + " cu ingredientele:\n";
                    ArrayList<MenuItem> currentMenuItems = customerOrders.get(currentOrder);
                    for(MenuItem currentMenuItem : currentMenuItems){
                        price += currentMenuItem.getPrice();
                        text +=  currentMenuItem.getName() + " " + currentMenuItem.getPrice() + "\n";
                    }
                }
            }
            text += "Pret Total: " + price;
            PrintWriter writer = null;
            writer = new PrintWriter(fileName, "UTF-8");
            writer.println(text);
            writer.close();
            assert value == menuItems.size() : "invalid operation";
            assert isWellFormed() : "is not well formed";
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * checks the consistency of the application
     * @return is it's consisten
     */
    protected boolean isWellFormed(){
        if (menuItems == null)
            return false;
        return true;
    }

    /**
     * getter for the customersOrders
     * @return return the hashMap
     */
    public HashMap<Order, ArrayList<MenuItem>> getCustomerOrders() {
        return customerOrders;
    }

    /**
     * a getter for the menu list
     * @return returns the menuList
     */
    public ArrayList<MenuItem> getMenuItems() {
        return menuItems;
    }

    /**
     * a setter used to initialize the observer
     * @param observer the chef
     */
    public void setObserver(Observer observer) {
        this.observer = observer;
        if (orders.size() > 0)
            notifyObserver(this, customerOrders.get(orders.get(orders.size() - 1)));
    }

    /**
     * a getter used to get the orders
     * @return the orders
     */
    public ArrayList<Order> getOrders() {
        return orders;
    }

}
