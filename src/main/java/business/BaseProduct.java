package business;

public class BaseProduct extends MenuItem{

    public BaseProduct(String name, int price) {
        this.setPrice(price);
        this.setName(name);
    }
}
